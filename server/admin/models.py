from flask.ext.bcrypt import check_password_hash, generate_password_hash
from flask.ext.login import UserMixin
from sqlalchemy.exc import IntegrityError
from ..db import db, ActiveModel


# Admin table
class Admin(ActiveModel, UserMixin, db.Model):
    __tablename__ = 'admins'

    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(32))
    last_name = db.Column(db.String(32))
    email = db.Column(db.String(255), nullable=False, unique=True)
    password = db.Column(db.String(255), nullable=False)
    active =  db.Column(db.Boolean, nullable=False, default=True)

    @classmethod
    def authenticate_user(cls, email, password):
        admin = cls.query.filter(cls.email == email).first()
        if not admin:
            return
        elif check_password_hash(admin.password, password):
            return admin

    @classmethod
    def create_admin(cls, email, password, first_name, last_name, **kwargs):
        try:
            admin = cls()
            admin.email = email
            admin.password = generate_password_hash(password)
            admin.first_name = first_name
            admin.last_name = last_name
            admin.save(**kwargs)
        except IntegrityError:
            db.session.rollback()
            return None
        except:
            return None

        return admin

    @property
    def full_name(self):
        return '{} {}'.format(self.first_name, self.last_name)

    def is_active(self):
        return self.active

    def __repr__(self):
        return '<User {}>'.format(self.full_name)
