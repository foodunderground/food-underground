from flask import Blueprint, redirect, request, render_template, url_for
from flask.ext.login import current_user, LoginManager, login_required, login_user, logout_user
from .models import Admin


admin = Blueprint('admin', __name__, template_folder='templates')
login_manager = LoginManager()


# Authorization via login manager
@login_manager.user_loader
def load_user(user_id):
    return Admin.query.get(user_id)


@admin.route('/login', methods=['GET', 'POST'])
def send_message():
    if current_user.is_authenticated():
        return redirect(url_for('dash.overview'))

    if request.method == 'POST':
        admin = Admin.authenticate_user(request.form['email'], request.form['password'])
        if not admin:
            # Incorrect email/password
            pass
        else:
            remember = request.form.get('remember') == 'on'
            login_user(admin, remember=remember)
            return redirect(url_for('dash.overview'))

    return render_template('admin/login.html')


@admin.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('general.index'))