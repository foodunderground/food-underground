DEBUG = True

# Email
CONTACT_EMAIL_ADDRESS = 'mattschmo@gmail.com'
MAILCHIMP_LIST_ID = '0557334f09'

# Secret Key for sessions - update on prod config
SECRET_KEY = '\x85X\xe8F\xbb$\xd9\xd5Z\xdfk\x91\xc4*\x08\xd3\x9b=*\r\xdd\xd5k\xb3'

# MySQL SQL-Alchemy set up
SQLALCHEMY_DATABASE_URI = 'mysql://fu:abcd1234@127.0.0.1:3306/fu_development_master'
SQLALCHEMY_MANAGE_DATABASE_URI = 'mysql://fu:abcd1234@127.0.0.1:3306'

# S3
S3_BUCKET = 'fugallerytest'
S3_HOST = 's3-us-west-2.amazonaws.com'

# Flask-WTF
WTF_CSRF_SECRET_KEY = 'wtf-foodunderground-for-life'

# Google Developers API ** Update locally
GOOGLE_KEY_FILEPATH = '/Users/mattschmoyer/Projects/prof/foodunderground/google_secret.json'