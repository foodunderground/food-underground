import json
from oauth2client.client import SignedJwtAssertionCredentials


# The scope for the OAuth2 request.
SCOPE = ['https://www.googleapis.com/auth/analytics.readonly']

# Defines a method to get an access token from the credentials object.
# The access token is automatically refreshed if it has expired.
def get_access_token(app):
    with open(app.config['GOOGLE_KEY_FILEPATH']) as key_file:
        key_data = json.load(key_file)

    credentials = SignedJwtAssertionCredentials(
        key_data['client_email'], key_data['private_key'], SCOPE)

    return credentials.get_access_token().access_token
