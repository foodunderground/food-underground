from flask import Blueprint, render_template
from ..events.models import Event
from ..gallery.models import Image


general = Blueprint('general', __name__, template_folder='templates')

@general.route('/')
def index():
    return render_template('general/index.html',
        recent_events=Event.get_recent_events(),
        upcoming_events=Event.get_upcoming_events())


@general.route('/team')
def team():
    return render_template('general/team.html')

@general.route('/contact')
def contact():
    return render_template('general/contact.html')


# Services
@general.route('/services')
def services():
    return render_template('general/services.html')

@general.route('/services/plated-dinners')
def services_plated_dinners():
    return render_template('general/plated_dinners.html')

@general.route('/services/workshops')
def services_workshops():
    return render_template('general/workshops.html')

@general.route('/services/cooks-canvas')
def services_cooks_canvas():
    return render_template('general/cooks_canvas.html')

@general.route('/services/popup-events')
def services_popup_events():
    return render_template('general/popup_events.html')