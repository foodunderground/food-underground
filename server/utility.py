import json
import requests
from datetime import datetime
from .app import app


EVENTS_URL = 'https://www.googleapis.com/calendar/v3/calendars/kak4e2tnnlmfala9eh72q95pr8@group.calendar.google.com/events'

def format_event_date(value):
    if value.get('dateTime'):
        date = datetime.strptime(value['dateTime'], '%Y-%m-%dT%H:%M:%S-04:00')
    else:
        date = datetime.strptime(value['date'], '%Y-%m-%d')
    return '{date:%B} {date.day}, {date.year}'.format(date=date)

def get_gcal_feed(max_results=5):
    params = {
        'key': app.config['GCAL_API_KEY'],
        'maxResults': max_results,
        'singleEvents': True,
        'timeMin': '{}T00:00:00-04:00'.format(datetime.now().strftime('%Y-%m-%d'))
    }
    events = json.loads(requests.get(EVENTS_URL, params=params).text)
    for event in events['items']:
        event['start_date'] = format_event_date(event['start'])
    return events['items']