// Smooth scrolling for UI elements page
$(document).ready(function(){
   $('a[href*=#buttons],a[href*=#panels], a[href*=#info-boards], a[href*=#navs], a[href*=#alerts], a[href*=#thumbnails], a[href*=#social], a[href*=#section-header],a[href*=#page-tip], a[href*=#block-header], a[href*=#tags]').bind("click", function(e){
    var anchor = $(this);
    $('html, body').stop().animate({
     scrollTop: $(anchor.attr('href')).offset().top
    }, 1000);
    e.preventDefault();
   });
   return false;
});

function toggle_subscribe_form() {
  $("#subscribe-icon").toggleClass('fa-search fa-times margin-2');
  $("#subscribe-box").toggleClass('show hidden animated fadeInUp');
}

$('#subscribe-form').submit(function() {
  $.post('/subscribe', $('#subscribe-form').serialize(), function(data) {
    if (data.success) {
      $("#subscribe-btn").notify(data.message, 'success');
    } else {
      $("#subscribe-btn").notify(data.message, 'error');
    }
    toggle_subscribe_form();
  });
  return false;
});

// Subscribe navbar button
$('#subscribe-btn, .subscribe-link').on('click', function() {
  toggle_subscribe_form();
  return false;
});