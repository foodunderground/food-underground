/*
 * jQuery File Upload Plugin JS Example 8.9.1
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global $, window */

$(function () {
    'use strict';

    $("#add-file-button").click(function() {
       $("#add-file-input").click();
    });

    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        dataType: 'json',
        done: function (e, data) {
          $.each(data.result.files, function (index, file) {
            $('<p/>').text(file.name).appendTo(document.body);
          });
        },
        url: 'add'
    });

    // Load existing files:
    $('#fileupload').addClass('fileupload-processing');
    $.ajax({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: $('#fileupload').fileupload('option', 'url'),
        dataType: 'json',
        context: $('#fileupload')[0]
    }).always(function () {
        console.log('COME ON');
        $(this).removeClass('fileupload-processing');
    }).done(function (result) {
        console.log(result);
        $(this).fileupload('option', 'done')
            .call(this, $.Event('done'), {result: result});
    });

});