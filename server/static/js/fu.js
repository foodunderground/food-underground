// jQuery for page scrolling feature - requires jQuery Easing plugin
(function() {
    function convert_hour(hour) {
        return ((hour + 11) % 12 + 1) + (hour < 12 ? 'am' : 'pm');
    }

    function show_event_details(selector, details, display) {
        $(selector).text(details ? display : '');
    }

    $(window).scroll(function() {
        if ($('.navbar').offset().top > 50) {
            $('.navbar-fixed-top').addClass('top-nav-collapse');
        } else {
            $('.navbar-fixed-top').removeClass('top-nav-collapse');
        }
    });

    $('a.page-scroll').bind('click', function(event) {
        $('html, body').stop().animate({
            scrollTop: $($(this).attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });

    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        // http://fullcalendar.io/docs/google_calendar/
        googleCalendarApiKey: 'AIzaSyBqgzXCEOX3u5EsLUsaqqFmYZC3z5y0dc0',
        eventSources:  [{
            googleCalendarId: 'e5h7mmeh0fi6dsjil1a9d355h8@group.calendar.google.com',
            color: 'black',
            className: 'fu-private-event'
        }, {
            googleCalendarId: 'kak4e2tnnlmfala9eh72q95pr8@group.calendar.google.com',
            color: 'green',
            className: 'fu-public-event'
        }],
        eventClick: function(event) {
            var start_hour = event.start.hour();
            var end_hour = event.end.hour();
            $("#external-events").fadeOut(function() {
                $('#event-title').html(event.title);
                show_event_details('#event-time', start_hour, 'When: ' + convert_hour(start_hour) + ' - ' + convert_hour(end_hour));
                show_event_details('#event-location', event.location, 'Where: ' + event.location);
                show_event_details('#event-description', event.description, 'What: ' + event.description);
            }).fadeIn();
            return false;
        },
        loading: function(bool) {
            $('#loading').toggle(bool);
        }
    });

    $('.navbar-collapse ul li a').click(function() {
        $('.navbar-toggle:visible').click();
    });

    var GammaSettings = {
        historyapi : false,
        viewport : [
            {width : 900, columns : 4},
            {width : 400, columns : 2},
            {width : 0, columns : 1}
        ]
    };
    Gamma.init(GammaSettings);
})();