import os

import rollbar
import rollbar.contrib.flask
from flask import Flask, render_template, got_request_exception
from flask.ext.migrate import Migrate

from .admin import admin, login_manager
from .db import db


app = None


def create_app():
    global app # So that app can imported with configurations
    app = Flask(__name__)
    app.config.from_pyfile('config_default.py')
    app.config.from_pyfile('config_prod.py', silent=True)

    # SQLAlchemy
    db.init_app(app)

    # Migrations
    Migrate(app, db)

    # Admin login
    login_manager.init_app(app)
    app.register_blueprint(admin)

    # Rollbar
    register_rollbar(app)

    from .general import general
    app.register_blueprint(general)

    from .events import events
    app.register_blueprint(events)

    from .menus import menus
    app.register_blueprint(menus)

    from .email import email
    app.register_blueprint(email)

    from .gallery import gallery
    app.register_blueprint(gallery)

    from .dash import dash
    app.register_blueprint(dash)

    register_errors(app)

    return app


def register_errors(app):
    # TODO - add 401 and 500
    @app.errorhandler(404)
    def page_not_found(e):
        return render_template('404.html'), 404


def register_rollbar(app):
    @app.before_first_request
    def init_rollbar():
        """init rollbar module"""
        rollbar.init(
            app.config['ROLLBAR_SERVER_TOKEN'],
            app.config['ROLLBAR_ENVIRONMENT'],
            # server root directory, makes tracebacks prettier
            root=os.path.dirname(os.path.realpath(__file__)),
            # flask already sets up logging
            allow_logging_basic_config=False)

        # send exceptions from app to rollbar, using flask's signal system.
        got_request_exception.connect(rollbar.contrib.flask.report_exception, app)
