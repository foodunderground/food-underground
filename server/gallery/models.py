from datetime import datetime
import urllib

from server.events.models import Event
from ..aws import upload_to_s3, delete_from_s3
from ..db import db, ActiveModel


# Images table
class Image(ActiveModel, db.Model):
    __tablename__ = 'images'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    description = db.Column(db.String(510), nullable=False)
    path = db.Column(db.String(2083), nullable=False)
    event_id = db.Column(db.Integer, db.ForeignKey('events.id'))
    menu_id = db.Column(db.Integer, db.ForeignKey('menus.id'))
    created_at = db.Column(db.DateTime, nullable=False)
    small_path = db.Column(db.String(2083))

    event = db.relationship('Event', foreign_keys='Image.event_id', uselist=False, backref='images')
    menu = db.relationship('Menu', foreign_keys='Image.menu_id', uselist=False, backref='images')

    @property
    def filename(self):
        return urllib.unquote(self.path.split('gallery/')[1])

    @classmethod
    def create_image(cls, name, description, path, event_id=None, menu_id=None, **kwargs):
        image = cls()
        image.name = name
        image.description = description
        image.event_id = event_id
        image.menu_id = menu_id
        image.path = path
        image.created_at = datetime.utcnow()
        image.save(**kwargs)

        dest_path = 'gallery/{}_{}'.format(name.lower().replace(' ', '_'), image.id)
        key = upload_to_s3(dest_path, filename=path, policy='public-read')
        image.path = key.generate_url(expires_in=0, query_auth=False)
        image.save()

        return image

    @classmethod
    def random_image(cls):
        return cls.query.order_by(db.func.rand()).first()

    @classmethod
    def remove_event(cls, event_id):
        images = cls.query.filter(cls.event_id == event_id).all()
        for image in images:
            image.event_id = None
            image.save()

    @classmethod
    def remove_image(cls, image_id):
        image = cls.query.get(image_id)
        # Check for events that use image as cover
        # Just switch to random image for now
        # TODO: change to a default image
        rando = Image.random_image()
        for event in Event.query.filter(Event.image_id == image.id):
            event.image_id = rando.id
            event.save()
        if image:
            delete_from_s3(image.path)
            image.remove()

    @classmethod
    def get_gallery_pagination(cls, page, per_page=8):
        return cls.query.join(cls.events).group_by(cls.id).paginate(page, per_page=per_page)

    def data_filters(self):
        if self.event:
            return self.event.e_type.data_filter[1:]
        return ''

    def get_similar_images(self, limit=6):
        return Image.query.filter(Image.id != self.id, Image.event_id==self.event_id).limit(limit)

    def __repr__(self):
        return '<Image (id={}, name={})>'.format(self.id, self.name)