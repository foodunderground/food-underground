from flask import Blueprint, render_template, abort
from .models import Image
from ..events.models import EventType


gallery = Blueprint('gallery', __name__, template_folder='templates', url_prefix='/gallery')


@gallery.route('/')
def view_gallery():
    images = Image.get_gallery_pagination(1)
    event_types = EventType.query.all()
    return render_template('gallery/gallery.html', images=images, event_types=event_types)


@gallery.route('/view/<int:image_id>')
def gallery_item(image_id):
    image = Image.query.get(image_id)
    if not image:
        abort(404)
    return render_template('gallery/gallery_item.html', image=image)


# Ajax loaded images for jscroll
@gallery.route('/<int:page>')
def gallery_page(page=1):
    images = Image.get_gallery_pagination(page)
    return render_template('gallery/page.html', images=images)