from flask import Blueprint, render_template, abort, request
from .models import Event


events = Blueprint('events', __name__, template_folder='templates', url_prefix='/events')

@events.route('/')
@events.route('/<int:page>')
def list_events(page=1):
    year = request.args.get('year')
    month = request.args.get('month')
    events = Event.get_timeline_events(page, year, month)
    archive = Event.get_archive()
    return render_template(
        'events/events.html',
        events=events,
        archive=archive,
        request_year=int(year) if year else None,
        request_month=int(month) if month else None
    )

@events.route('/view/<int:event_id>')
def view_event(event_id):
    event = Event.query.get(event_id)
    if not event:
        abort(404)
    return render_template('events/view_event.html', event=event)