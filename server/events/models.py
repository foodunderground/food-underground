import itertools
from datetime import datetime, date
from calendar import month_abbr
from ..db import db, ActiveModel


class Event(ActiveModel, db.Model):
    __tablename__ = 'events'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    description = db.Column(db.String(765), nullable=False)
    location = db.Column(db.String(128))
    date = db.Column(db.Date, nullable=False)
    event_type = db.Column(db.Integer, db.ForeignKey('event_types.id'), nullable=False)
    image_id = db.Column(db.Integer, db.ForeignKey('images.id'), nullable=False)
    menu_id = db.Column(db.Integer, db.ForeignKey('menus.id'))
    bpt_id = db.Column(db.Integer)
    public = db.Column(db.Boolean, nullable=False, default=True)
    active = db.Column(db.Boolean, nullable=False, default=True)
    archived = db.Column(db.Boolean, nullable=False, default=False)

    cover_image = db.relationship('Image', foreign_keys='Event.image_id', uselist=False, backref='events')
    e_type = db.relationship('EventType', foreign_keys='Event.event_type', uselist=False, backref='events')

    @classmethod
    def get_recent_events(cls, limit=4):
        today = datetime.now().strftime('%Y-%m-%d')
        recent_events = iter(
            cls.query
                .filter(cls.date < today, cls.active == True, cls.archived == False)
                .order_by(cls.date.desc())
                .limit(limit)
        )

        # Group recent events into tuples of 2
        while True:
            chunk = tuple(itertools.islice(recent_events, 2))
            if not chunk:
                return
            yield chunk

    @classmethod
    def get_upcoming_events(cls, limit=5):
        today = datetime.now().strftime('%Y-%m-%d')
        return (
            cls.query
                .filter(cls.date >= today, cls.active == True, cls.archived == False)
                .order_by(cls.date)
                .limit(limit)
        )

    @classmethod
    def random_event(cls):
        return cls.query.order_by(db.func.rand()).first()

    @classmethod
    def get_archive(cls):
        """Get data structure for date archive output
        :return: { year: { month: (month_abbr, count) }}
        """
        archive = {}
        results = db.session.query(db.func.year(cls.date), db.func.month(cls.date), db.func.count(cls.id)). \
            filter(cls.active == True, cls.archived == False). \
            group_by(db.func.year(cls.date), db.func.month(cls.date))

        for row in results:
            year = archive.setdefault(row[0], {})
            year[row[1]] = (month_abbr[row[1]], row[2])

        return archive

    @classmethod
    def get_timeline_events(cls, page=1, year=None, month=None):
        events = cls.query.filter(cls.active == True, cls.archived == False)
        if year:
            events = events.filter(db.func.extract('year', cls.date) == year)
        if month:
            events = events.filter(db.func.extract('month', cls.date) == month)
        return events.order_by(Event.date.desc()).paginate(page, 4)

    def is_upcoming(self):
        return self.date >= date.today()

    def gallery_images(self):
        for image in self.images:
            yield image
        yield self.cover_image

    def format_date(self, format='%b %d, %Y'):
        return self.date.strftime(format)

    def get_similar_events(self, limit=6):
        return Event.query.filter(
            Event.id != self.id,
            Event.event_type == self.event_type,
            Event.active == True,
            Event.archived == False).limit(limit)

    def __repr__(self):
        return '<Event {}>'.format(self.name)


class EventType(ActiveModel, db.Model):
    __tablename__ = 'event_types'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    description = db.Column(db.String(255))

    @property
    def data_filter(self):
        return '.cat-{}'.format(self.name.lower().replace(' ', '-'))

    def __repr__(self):
        return '<Event Type {}>'.format(self.name)
