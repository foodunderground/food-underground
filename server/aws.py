import boto
from boto.s3.key import Key
from app import app


def get_s3_bucket():
    conn = boto.connect_s3(app.config['AWS_ACCESS_KEY_ID'],
        app.config['AWS_SECRET_ACCESS_KEY'], host=app.config['S3_HOST'])
    return conn.get_bucket(app.config['S3_BUCKET'])

def upload_to_s3(dest_filepath, filename=None, policy=None, metadata={}):
    key = Key(get_s3_bucket())
    key.key = dest_filepath
    key.update_metadata(metadata)
    if filename:
        key.set_contents_from_filename(filename, policy=policy)
    else:
        raise Exception('File name needs to be specified.')
    return key

def delete_from_s3(remote_filepath):
    s3_bucket = get_s3_bucket()
    return s3_bucket.delete_key(remote_filepath)

def get_key_from_s3(bucket, remote_filepath):
    key = Key(bucket)
    key.key = remote_filepath
    return key
