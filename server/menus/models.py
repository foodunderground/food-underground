from ..db import db, ActiveModel


class Menu(ActiveModel, db.Model):
    __tablename__ = 'menus'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), nullable=False)
    event_id = db.Column(db.Integer, db.ForeignKey('events.id'), nullable=False)
    image_id = db.Column(db.Integer, db.ForeignKey('images.id'))
    created_at = db.Column(db.DateTime, nullable=False)

    cover_image = db.relationship('Image', foreign_keys='Menu.image_id', uselist=False)
    event = db.relationship('Event', foreign_keys='Menu.event_id', uselist=False, backref='menu')

    def __repr__(self):
        return '<Menu {}>'.format(self.name)


class MenuCourse(ActiveModel, db.Model):
    __tablename__ = 'menu_courses'
    __table_args__ = (db.UniqueConstraint('menu_id', 'course_order', name='menu_order'),)

    id = db.Column(db.Integer, primary_key=True)
    menu_id = db.Column(db.Integer, db.ForeignKey('menus.id'), nullable=False)
    description = db.Column(db.String(255), nullable=False)
    course_order = db.Column(db.Enum('1', '2', '3', '4', '5', '6', '7', '8', name='course_orders'),
        nullable=False, default='1')
    image_id = db.Column(db.Integer, db.ForeignKey('images.id'))

    menu = db.relationship('Menu', backref="menu_courses")

    def __repr__(self):
        return '<Menu Course {}>'.format(self.name)
