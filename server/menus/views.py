from flask import Blueprint, render_template, abort
from .models import Menu


menus = Blueprint('menus', __name__, template_folder='templates', url_prefix='/menus')

@menus.route('/')
def list_menus():
    return render_template('menus/menus.html', menus=Menu.query.all())

@menus.route('/<int:menu_id>')
def view_menu(menu_id):
    menu = Event.query.get(menu_id)
    if not menu:
        abort(404)
    return render_template('menus/view_menu.html', menu=menu)