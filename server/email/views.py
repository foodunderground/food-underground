from flask import Blueprint, request, jsonify, render_template
from mailsnake import ListAlreadySubscribedException
from .emailer import subscribe_email, send_contact_message


email = Blueprint('email', __name__, template_folder='templates')

@email.route('/send_message', methods=['POST'])
def send_message():
    form = request.form
    send_contact_message(form['message'], form['email'], form['name'])
    return render_template('email/sent_message.html')

# ajax
@email.route('/subscribe', methods=['POST'])
def subscribe():
    try:
        subscribe_email(request.form['email'])
        return jsonify(success=True, message='Thanks for signing up!')
    except ListAlreadySubscribedException:
        return jsonify(success=False, message='Already subscribed')
    except:
        return jsonify(success=False, message='An error occured. Sorry!')