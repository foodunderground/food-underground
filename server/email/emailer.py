from .email_apis import mailchimp, mandrill
from server.app import app


def subscribe_email(email):
    mailchimp.listSubscribe(id=app.config['MAILCHIMP_LIST_ID'],
                            email_address=email, double_optin=False)

def send_contact_message(text, from_email, name):
    mandrill.messages.send(message={
        'text': text,
        'subject': 'New Message From foodunderground.net',
        'from_email': from_email,
        'from_name': name,
        'to':[{
            'email': app.config['CONTACT_EMAIL_ADDRESS']
        }]
    })