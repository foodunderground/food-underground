from mailsnake import MailSnake
from server.app import app


mailchimp = MailSnake(app.config['MAILCHIMP_API_KEY'])
mandrill = MailSnake(app.config['MANDRILL_API_KEY'], api='mandrill')