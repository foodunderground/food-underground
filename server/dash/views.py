import os
from flask import Blueprint, render_template, url_for, redirect, flash, current_app, request, jsonify
from flask.ext.login import login_required
from werkzeug import secure_filename
from .forms import UploadImageForm, EditImageForm, EventForm, MenuForm, IssueForm
from ..bitbucket import post_new_issue
from ..events.models import Event
from ..gallery.models import Image
from ..menus.models import Menu
from ..service_account import get_access_token

from server.upload_file import uploadfile

dash = Blueprint('dash', __name__, template_folder='templates', url_prefix='/dash')

ALLOWED_EXTENSIONS = ('png', 'jpg', 'jpeg', 'gif')


@dash.route('/')
@login_required
def overview():
    ga_service_token = get_access_token(current_app)
    return render_template('dash/overview.html', ga_service_token=ga_service_token)


### Events ###

@dash.route('/events')
@dash.route('/events/<int:page>')
@login_required
def list_events(page=1):
    search_query = request.args.get('search')
    events = Event.query.filter(Event.archived==False)
    if search_query:
        events = events.filter(Event.name.like('%{}%'.format(search_query)))
    events = events.paginate(page, per_page=10)
    return render_template('dash/events.html', events=events, search_query=search_query)


@dash.route('/events/add', methods=['GET', 'POST'])
@login_required
def add_event():
    form = EventForm()
    form.add_image_choices()
    form.add_event_type_choices()
    if form.validate_on_submit():
        event = Event(
            name=form.name.data,
            description=form.description.data,
            location=form.location.data,
            date=form.date.data,
            event_type=form.event_type.data,
            image_id=form.image_id.data,
            menu_id=form.menu_id.data,
            active=form.active.data,
            bpt_id=form.bpt_id.data
        )
        event.save()
        return redirect(url_for('.edit_event', event_id=event.id))
    return render_template('dash/add_event.html', form=form)


@dash.route('/events/edit/<int:event_id>', methods=['GET', 'POST'])
@login_required
def edit_event(event_id):
    event = Event.query.get(event_id)
    form = EventForm(obj=event)
    form.add_image_choices()
    form.add_event_type_choices()
    if form.validate_on_submit():
        form.populate_obj(event)
        event.save()
        flash('Successfully updated event: {}'.format(event.name), category='info')
    return render_template('dash/edit_event.html', event=event, form=form)


@dash.route('/events/archive/<int:event_id>', methods=['POST'])
@login_required
def archive_event(event_id):
    event = Event.query.get(event_id)
    if event:
        event.archived = True
        event.save()
        Image.remove_event(event_id)
    return redirect(url_for('.list_events'))


### Images ###

@dash.route('/images')
@dash.route('/images/<int:page>')
@login_required
def list_images(page=1):
    search_query = request.args.get('search')
    images = Image.query
    if search_query:
        images = images.filter(Image.name.like('%{}%'.format(search_query)))
    images = images.paginate(page, per_page=10)
    return render_template('/dash/list_images.html', images=images, search_query=search_query)


@dash.route('/images/add', methods=['GET', 'POST'])
@login_required
def add_image():
    form = UploadImageForm()
    if request.method == 'POST': #  if form.validate_on_submit():
        image_file = request.files['file']
        filename = secure_filename(image_file.filename)
        full_path = '/tmp/{}'.format(filename)  # TODO - use os join
        image_file.save(full_path)
        image = Image.create_image(name=form.name.data, description=form.description.data, path=full_path)
        # get file size after saving
        size = os.path.getsize(full_path)
        # return json for js call back
        result = uploadfile(name=image.path, type=image_file.content_type, size=size)
        return jsonify(files=result.get_file())

    return render_template('dash/add_image.html', form=form)


@dash.route('/images/edit/<int:image_id>', methods=['GET', 'POST'])
@login_required
def edit_image(image_id):
    image = Image.query.get(image_id)
    form = EditImageForm(obj=image)
    form.add_event_choices()
    form.add_menu_choices()
    if form.validate_on_submit():
        form.populate_obj(image)
        image.save()
        flash('Successfully updated image: {}'.format(image.name), category='info')
    return render_template('dash/edit_image.html', image=image, form=form)


@dash.route('/images/remove/<int:image_id>', methods=['POST'])
@login_required
def remove_image(image_id):
    Image.remove_image(image_id)
    return redirect(url_for('.list_images'))

### Menus ###

@dash.route('/menus')
def list_menus():
    menus = Menu.query.all()
    return render_template('/dash/list_menus.html', menus=menus)


@dash.route('/menus/add', methods=['GET', 'POST'])
@login_required
def add_menu():
    form = MenuForm()
    if form.validate_on_submit():
        menu = Menu()
        menu.save()
        return redirect(url_for('.edit_menu', menu_id=menu.id))
    images = Image.query.all()
    return render_template('dash/add_menu.html', form=form, images=images)


@dash.route('/menus/<int:menu_id>', methods=['GET', 'POST'])
def edit_menu(menu_id):
    menu = Menu.query.get(menu_id)
    form = MenuForm(obj=menu)
    form.add_event_choices()
    if form.validate_on_submit():
        form.populate_obj(menu)
        menu.save()
        flash('Successfully updated {}'.format(menu.name))
    return render_template('dash/edit_menu.html', menu=menu, form=form)


### Issues ###

@dash.route('/issues', methods=['GET', 'POST'])
def issues():
    form = IssueForm()
    if form.validate_on_submit():
        post_new_issue(form.title.data, form.description.data, form.priority.data, form.kind.data)
        flash('Successfully posted the new issue')
    return render_template('dash/issues.html', form=form)
