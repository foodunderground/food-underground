from flask_wtf import Form
from flask_wtf.file import FileField, FileAllowed, FileRequired
from wtforms import StringField, IntegerField, DateField, BooleanField, SelectField
from wtforms.validators import DataRequired, Optional

from ..events.models import Event, EventType
from ..gallery.models import Image
from ..menus.models import Menu


class BaseFoodUndergroundForm(Form):
    name = StringField('Name', validators=[DataRequired()])
    description = StringField('Description', validators=[DataRequired()])


class AddChoicesMixin():
    def add_event_choices(self):
        self.event_id.choices = [(e.id, e.name) for e in Event.query.order_by('name')]

    def add_menu_choices(self):
        self.menu_id.choices = [(m.id, m.name) for m in Menu.query.order_by('name')]

    def add_image_choices(self):
        self.image_id.choices = [(i.id, i.name) for i in Image.query.order_by('name')]


### Images ###

class UploadImageForm(BaseFoodUndergroundForm):
    image = FileField('Image', validators=[
        FileRequired(),
        FileAllowed(['jpg', 'png', 'jpeg'], 'JPG or PNG only!')
    ])


class EditImageForm(AddChoicesMixin, BaseFoodUndergroundForm):
    event_id = SelectField('Event', validators=[Optional()], coerce=int)
    menu_id = SelectField('Menu', validators=[Optional()], coerce=int)


### Events ###

class EventForm(AddChoicesMixin, BaseFoodUndergroundForm):
    date = DateField('Date', validators=[DataRequired()], format='%m/%d/%Y')
    location = StringField('Location', validators=[Optional()])
    event_type = SelectField('Event Type', validators=[DataRequired()], coerce=int)
    image_id = SelectField('Image', validators=[DataRequired()], coerce=int)
    menu_id = IntegerField('Menu', validators=[Optional()])
    bpt_id = IntegerField('Brown Paper Ticket ID', validators=[Optional()])
    public = BooleanField('Public', validators=[Optional()], default='checked')
    active = BooleanField('Active', validators=[Optional()], default='checked')

    def add_event_type_choices(self):
        self.event_type.choices = [(et.id, et.name) for et in EventType.query.order_by('name')]


### Menus ###

class MenuForm(AddChoicesMixin, Form):
    name = StringField('Name', validators=[DataRequired()])
    event_id = SelectField('Event', validators=[DataRequired()], coerce=int)
    image_id = IntegerField('Image', validators=[Optional()])


class CourseForm(AddChoicesMixin, Form):
    menu_id = SelectField('Menu', validators=[DataRequired()], coerce=int)
    description = StringField('Description', validators=[DataRequired()])
    course_order = ()
    image_id = IntegerField('Image', validators=[Optional()])


### Issues ###

class IssueForm(Form):
    title = StringField('Title', validators=[DataRequired()])
    description = StringField('Description', validators=[DataRequired()])
    priority = SelectField('Priortity', choices=[
        ('trivial', 'trivial'),
        ('minor', 'minor'),
        ('major', 'major'),
        ('critical', 'critical'),
        ('blocker', 'blocker')
    ])
    kind = SelectField('Kind', choices=[
        ('bug', 'bug'),
        ('enhancement', 'enhancement'),
        ('proposal', 'proposal'),
        ('task', 'task')
    ])