import requests
from requests_oauthlib import OAuth1
from app import app


BITBUCKET_URL = 'https://bitbucket.org/api/1.0/'
AUTH = OAuth1(app.config['BITBUCKET_OAUTH_KEY'], app.config['BITBUCKET_OAUTH_SECRET'])


def post_new_issue(title, description, priority='trivial', kind='bug'):
    data = {
        'title': title,
        'content': description,
        'priority': priority,
        'kind': kind,
        'responsible': 'mschmoyer'
    }
    response = requests.post('{}repositories/foodunderground/food-underground/issues/'.format(BITBUCKET_URL),
        data=data, auth=AUTH)
    return response