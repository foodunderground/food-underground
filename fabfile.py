import requests
from dogapi.fab import setup as dog_setup, notify
from fabric.api import *
from server.config_prod import ROLLBAR_SERVER_TOKEN, DATADOG_API_KEY


env.use_ssh_config = True
env.hosts = ['food']
PROJECT_DIR = '/home/ubuntu/food-underground'
dog_setup(DATADOG_API_KEY)


def pull_code():
    run('git pull origin master')
    run('../env/bin/pip install -r requirements.txt')


def push():
    local('git push origin master')


def restart_service():
    run("sudo supervisorctl restart food_underground")


def upgrade_db():
    with cd(PROJECT_DIR):
        run('/home/ubuntu/env/bin/python manage.py db upgrade')


def rollbar_record_deploy():
    local_username = local('whoami', capture=True)
    # fetch last committed revision in the locally-checked out branch
    revision = local('git log -n 1 --pretty=format:"%H"', capture=True)

    resp = requests.post('https://api.rollbar.com/api/1/deploy/', {
        'access_token': ROLLBAR_SERVER_TOKEN,
        'environment': 'production',
        'local_username': local_username,
        'revision': revision
    }, timeout=3)

    if resp.status_code == 200:
        print 'Deploy recorded successfully.'
    else:
        print 'Error recording deploy: {}'.format(resp.text)


@notify
@task
def deploy():
    push()

    with cd(PROJECT_DIR):
        pull_code()

    upgrade_db()
    restart_service()
    rollbar_record_deploy()


@notify
@task
def upgrade_requirements():
    with cd(PROJECT_DIR):
        run('source ../env/bin/activate && pip install -U -r requirements.txt')