#!/usr/bin/env python
from server.app import create_app

create_app()

import os

from PIL import Image as PILImage
from server.app import app
from server.aws import get_s3_bucket, upload_to_s3, get_key_from_s3
from server.db import db
from flask.ext.migrate import MigrateCommand
from flask.ext.script import Manager

# TODO - figure out a better way for manager to find models
from server.admin.models import Admin
from server.gallery.models import Image


manager = Manager(app)
manager.add_command('db', MigrateCommand)

@manager.option('-e', '--email', help='Account email address')
@manager.option('-p', '--password', help='Account password')
@manager.option('-f', '--first_name', help='Admin first name')
@manager.option('-l', '--last_name', help='Admin last name')
def create_admin(email, password, first_name, last_name):
    admin = Admin.create_admin(email, password, first_name=first_name, last_name=last_name)
    print('Admin {} created.'.format(admin.full_name))

def process_image(filename):
    img = PILImage.open(filename)
    ogn_size = img.size  # gives you a tuple (x, y)

    # base on ogn_size do proportional resizing, let's say by 50%. also apply the recommended
    # downsizing high quality filter ANTIALIAS
    image_half = img.resize((int(ogn_size[0] * 0.5), int(ogn_size[1] * 0.5)))

    # more stuff to do, such as optimization on save
    new_image_name = '{}_small.jpg'.format(filename)
    image_half.save(new_image_name, optimize=True, quality=80)
    return new_image_name

@manager.command
def resize_s3_images():
    bucket = get_s3_bucket()
    rs = bucket.list(prefix='gallery')
    for image in Image.query.filter(db.column('small_path') == None).all():
        print(image.name)
        filename = image.filename

        print('getting {}\n'.format(filename))
        key = get_key_from_s3(bucket, 'gallery/{}'.format(image.filename))
        with open(filename, "w") as fp:
            key.get_file(fp)
            fp.flush()

        print('processing {}\n'.format(filename))
        new_image = process_image(filename)
        os.remove(filename)
        new_filename = 'small_gal/{}.jpg'.format(filename)

        print("uploading {}\n".format(new_filename))
        key = upload_to_s3(new_filename, new_image, policy='public-read')
        os.remove(new_image)
        image.small_path = key.generate_url(expires_in=0, query_auth=False)
        image.save()


if __name__ == '__main__':
    manager.run()